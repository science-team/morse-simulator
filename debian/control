Source: morse-simulator
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Sylvestre Ledru <sylvestre@debian.org>,
           Séverin Lemaignan <severin@guakamole.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               dh-python,
               python3-all,
               python3-dev,
               python3-sphinx,
               pkg-config,
               python3-setuptools
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/science-team/morse-simulator
Vcs-Git: https://salsa.debian.org/science-team/morse-simulator.git
Homepage: http://morse-simulator.github.io/
Rules-Requires-Root: no

Package: morse-simulator
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         blender [!armel !armhf !i386 !mipsel],
         ${python3:Depends},
         python3-morse-simulator (= ${binary:Version}),
         morse-simulator-data,
         python3-numpy
Recommends: morse-simulator-doc
Conflicts: morse
Description: Multi-OpenRobot Simulation Engine
 List of morse features:
  * Versatile simulator for generic mobile robots simulation
    (single or multi robots),
  * Realistic and dynamic environments (interaction with other agents like
    humans or objects),
  * Based on well known and widely adopted open source projects (Blender for 3D
    rendering + UI, Bullet for physics simulation, dedicated robotic
    middlewares for communications + robot hardware support),
  * Seamless workflow: since the simulator rely on Blender for both modeling
    and the real time 3D engine, creating and modifying a simulated scene is
    straightforward.
  * Entirely scriptable in Python,
  * Adaptable to various level of simulation realism (for instance the
    simulation of exteroceptive sensors like cameras or a direct access to
    higher level representations of the world, like labeled artifacts),
  * Currently compatible with ROS, YARP and the LAAS OpenRobots robotics
    frameworks,
  * Easy to integrate to other environments via a simple socket interface,
  * Fully open source, BSD license.

Package: morse-simulator-data
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Multi-OpenRobot Simulation Engine
 List of morse features:
  * Versatile simulator for generic mobile robots simulation
    (single or multi robots),
  * Realistic and dynamic environments (interaction with other agents like
    humans or objects),
  * Based on well known and widely adopted open source projects (Blender for 3D
    rendering + UI, Bullet for physics simulation, dedicated robotic
    middlewares for communications + robot hardware support),
  * Seamless workflow: since the simulator rely on Blender for both modeling
    and the real time 3D engine, creating and modifying a simulated scene is
    straightforward.
  * Entirely scriptable in Python,
  * Adaptable to various level of simulation realism (for instance the
    simulation of exteroceptive sensors like cameras or a direct access to
    higher level representations of the world, like labeled artifacts),
  * Currently compatible with ROS, YARP and the LAAS OpenRobots robotics
    frameworks,
  * Easy to integrate to other environments via a simple socket interface,
  * Fully open source, BSD license.
 .
 This package contains morse data.

Package: morse-simulator-doc
Architecture: all
Section: doc
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libjs-jquery,
         libjs-underscore
Description: Multi-OpenRobot Simulation Engine - Documentation
 List of morse features:
  * Versatile simulator for generic mobile robots simulation
    (single or multi robots),
  * Realistic and dynamic environments (interaction with other agents like
    humans or objects),
  * Based on well known and widely adopted open source projects (Blender for 3D
    rendering + UI, Bullet for physics simulation, dedicated robotic
    middlewares for communications + robot hardware support),
  * Seamless workflow: since the simulator rely on Blender for both modeling
    and the real time 3D engine, creating and modifying a simulated scene is
    straightforward.
  * Entirely scriptable in Python,
  * Adaptable to various level of simulation realism (for instance the
    simulation of exteroceptive sensors like cameras or a direct access to
    higher level representations of the world, like labeled artifacts),
  * Currently compatible with ROS, YARP and the LAAS OpenRobots robotics
    frameworks,
  * Easy to integrate to other environments via a simple socket interface,
  * Fully open source, BSD license.
 .
 This package contains the documentation.

Package: python3-morse-simulator
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends}
Conflicts: morse
Description: Multi-OpenRobot Simulation Engine
 List of morse features:
  * Versatile simulator for generic mobile robots simulation
    (single or multi robots),
  * Realistic and dynamic environments (interaction with other agents like
    humans or objects),
  * Based on well known and widely adopted open source projects (Blender for 3D
    rendering + UI, Bullet for physics simulation, dedicated robotic
    middlewares for communications + robot hardware support),
  * Seamless workflow: since the simulator rely on Blender for both modeling
    and the real time 3D engine, creating and modifying a simulated scene is
    straightforward.
  * Entirely scriptable in Python,
  * Adaptable to various level of simulation realism (for instance the
    simulation of exteroceptive sensors like cameras or a direct access to
    higher level representations of the world, like labeled artifacts),
  * Currently compatible with ROS, YARP and the LAAS OpenRobots robotics
    frameworks,
  * Easy to integrate to other environments via a simple socket interface,
  * Fully open source, BSD license.
 .
 This package contains the Python extension.
